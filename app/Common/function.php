<?php

function return_error($msg='请求失败'){
    $data = [
            'code'=>500,
            'msg'=>$msg
    ];
    return response()->json($data);
}

function return_success($data = [],$msg = "请求成功"){
    $data = [
        'code'=>200,
        'msg'=>$msg,
        'data'=>$data
    ];
    return response()->json($data);
}