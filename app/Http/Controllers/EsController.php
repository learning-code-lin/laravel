<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Elasticsearch\ClientBuilder;

class EsController extends Controller{
    public $client  = '';
    private $index = '';
    private $type = "_doc";
    public function __construct(){
        $hosts = [
            '121.4.181.55:9200',         // IP + Port
        ];
        $this->index = "lin_index";
        $this->client = ClientBuilder::create()           // Instantiate a new ClientBuilder
            ->setHosts($hosts)      // Set the hosts
            ->build();  
    }

    //创建索引
    public function create_index(){
        $params = [
            'index' => 'lin_index'
        ];
        // Create the index
        $response = $this->client->indices()->create($params);
        return response()->json($response);
    }

    //获取索引 只能判断存在不存在
    public function get_index(){
        $params = ['index' => 'lin_index'];
        $response = $this->client->indices()->getSettings($params);
        return response()->json($response);
    }

    //删除索引请求
    public function delete_index(){
        $params = ['index' => 'lin_index'];
        $response = $this->client->indices()->delete($params);
        return response()->json($response);
    }

    //添加文档
    public function create_doc(){
        $params = [
            'index' => $this->index,
            'type'=>$this->type,
            'id' => 4,
            'body' => $this->user_data("小代子",16)
        ];
        $response = $this->client->index($params);
        return response()->json($response);
    }

    //批量添加文档
    public function bulk_create_doc(){
        for($i = 0; $i < 100; $i++) {
            $params['body'][] = [
                'index' => [
                    '_index' => 'lin_index',
                    '_type' => '_doc',
                    '_id' => $i,
                ]
            ];
        
            $params['body'][] = $this->user_data("test".$i,25+$i);
        }
        $responses = $this->client->bulk($params);
        return return_success($responses);
    }

    //获取文档
    public function get_doc(){
        $params = [
            'index' => 'lin_index',
            'type' => '_doc',
            'id' => '1'
        ];
        $exists = $this->client->exists($params);
        if(!$exists){
            return return_error();
        }
        
        $response = $this->client->get($params);
        return return_success($response);
    }

    //更新文档
    public function update_doc(){
        $params = [
            'index' => 'lin_index',
            'type' => '_doc',
            'id' => '1',
            'body' => [
                'doc' => $this->user_data("代林-PHP",26)
            ]
        ];
        
        // Update doc at /my_index/my_type/my_id
        $response = $this->client->update($params);
        return return_success($response);
    }

    //删除文档记录
    public function delete_doc(){
        $params = [
            'index' => 'lin_index',
            'type' => '_doc',
            'id' => '1'
        ];
        
        // Delete doc at /my_index/my_type/my_id
        $response = $this->client->delete($params);
        return return_success($response);
    }

    //搜索文档
    public function serach_doc(){
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'size'=>2,
            'body' => [
                'highlight' => [
                    'pre_tags' => ["<span style='color:red'>"],
                    'post_tags' => ["</span>"],
                    'fields' => [
                        "name" => new \stdClass()
                    ]
                ],
                'query' => [
                    'match' => [
                        'name' => '代林'
                    ]
                ]
            ]
        ];
        $results = $this->client->search($params);
        $res = $results['hits']['hits'];
        $list = [];
        foreach($res as $v){
            $v['_source']['name'] = $v['highlight']['name'][0];
            $list[] = $v['_source'];
        }
        return return_success($list);
    }
    

    private function user_data($name,$age){
        return [
            'name'=>$name,
            'age'=>$age
        ];
    }
}