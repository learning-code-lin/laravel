<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>京东购物</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>

<body>
    <div class="search_box">
        <form>
            <div class="row">
                <div class="col-md-4">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="请输入商品名称">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-default">搜索</button>
                </div>
            </div>
        </form>
    </div>

    <div class="product_box">
        <div class="product_item">
            <img src="https://img12.360buyimg.com/n7/jfs/t1/189884/1/14389/554845/60f91f0dE6ca81c54/cc1c01de736dce02.jpg" alt="" class="img-rounded">
            <h5>华为手机</h5>
            <p class="text-success">￥3999</p>
        </div>
    </div>

</body>

</html>
<style>
    .search_box {
        padding: 20px;
    }
    .product_box{
        padding: 20px;
        text-align: center;
    }
    .product_item{
        width: 240px;
        padding: 10px;
        border: 1px #ccc solid;
        margin: 0 5px;
    }
</style>