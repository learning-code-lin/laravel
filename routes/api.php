<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/create_index', "EsController@create_index");   //创建索引
Route::get('/get_index', "EsController@get_index");         //获取索引
Route::get('/delete_index', "EsController@delete_index");         //获取索引
Route::get('/create_doc', "EsController@create_doc");         //创建文档
Route::get('/bulk_create_doc', "EsController@bulk_create_doc");         //批量创建文档
Route::get('/get_doc', "EsController@get_doc");         //获取文档
Route::get('/update_doc', "EsController@update_doc");         //更新文档
Route::get('/delete_doc', "EsController@delete_doc");         //删除文档
Route::get('/serach_doc', "EsController@serach_doc");         //搜索文档
